import React from 'react';

const Navbar = () => {
    return (
        <div className='navbar navbar-light bg-primary'
            style={{ width: '100%' }}
        >
            <a className='navbar-brand text-light mr-auto' href='#'>CoronaTracker</a>
        </div >
    )
}

export default Navbar;