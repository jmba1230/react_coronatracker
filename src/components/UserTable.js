import React from 'react';
import { Table, Button } from 'reactstrap';

const UserTable = props => {
    return (
        <>
            <dl className='col my-2'>
                <dt><span className='badge-success'>Low Risk</span></dt>
                <dd>Age Group: 0-40 Years Old</dd>
                <dt><span className='badge-danger'>High Risk</span></dt>
                <dd>Age Group: 41-100+ Years Old &amp; With Pre-Existing Condition</dd>
            </dl>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Condition</th>
                        <th>Job</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {props.users.length > 0 ? (
                        props.users.map(user => {
                            const { id, name, age, gender, condition, job, address } = user;
                            return (
                                <tr
                                    key={id}
                                    style={{ color: user.age >= 0 && user.age <= 40 ? 'green' : 'red' }}
                                >
                                    <td>{id}</td>
                                    <td>{name}</td>
                                    <td>{age}</td>
                                    <td>{gender}</td>
                                    <td
                                        style={{ color: user.condition === 'Healthy' ? 'green' : 'red' }}
                                    >{condition}</td>
                                    <td>{job}</td>
                                    <td>{address}</td>
                                    <td>
                                        <Button
                                            color='danger'
                                            className='mx-1'
                                            onClick={() => props.deleteUser(id)}
                                        >Delete</Button>
                                        <Button
                                            color='info'
                                            onClick={() => props.editUser(id, user)}
                                        >Edit</Button>
                                    </td>
                                </tr>
                            )
                        })
                    ) : (
                            <tr>
                                <td colSpan={8}>No users found!</td>
                            </tr>
                        )
                    }

                </tbody>
            </Table>
        </>
    )
}

export default UserTable;