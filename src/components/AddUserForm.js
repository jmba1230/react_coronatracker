import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

const AddUserForm = props => {
    const initUser = {
        id: null,
        name: '',
        age: '',
        gender: '',
        condition: '',
        job: '',
        address: ''
    };

    const [user, setUser] = useState(initUser);

    const handleChange = e => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    }

    const handleSubmit = e => {
        e.preventDefault();
        if (user.name && user.age && user.gender && user.condition && user.job && user.address) props.addUser(user)
        setUser(initUser);
    }



    return (
        <Form>
            <FormGroup className='form-inline'>
                <Label
                    className='mr-2'
                >Name:</Label>
                <Input
                    className='mr-5'
                    placeholder='enter your name'
                    type='text'
                    name='name'
                    value={user.name}
                    onChange={handleChange}
                />

                <Label
                    className='mr-2'
                >Age:</Label>
                <Input
                    className='mr-5'
                    placeholder='enter your age'
                    type='text'
                    name='age'
                    value={user.age}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup className='form-inline'>
                <Label
                    className='mr-2'
                >Gender:</Label>
                <Input
                    className='mr-5'
                    placeholder='enter your gender'
                    type='text'
                    name='gender'
                    value={user.gender}
                    onChange={handleChange}
                />

                <Label
                    className='mr-2'
                >Condition:</Label>
                <Input
                    className='mr-5'
                    placeholder='enter your condition'
                    type='text'
                    name='condition'
                    value={user.condition}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup className='form-inline'>
                <Label
                    className='mr-2'
                >Job:</Label>
                <Input
                    className='mr-5'
                    placeholder='enter your job'
                    type='text'
                    name='job'
                    value={user.job}
                    onChange={handleChange}
                />

                <Label
                    className='mr-2'
                >Address:</Label>
                <Input
                    placeholder='enter your address'
                    type='text'
                    name='address'
                    value={user.address}
                    onChange={handleChange}
                />
            </FormGroup>
            <Button
                color='success'
                type='submit'
                onClick={handleSubmit}
            >Add user</Button>
        </Form>
    )
}

export default AddUserForm;