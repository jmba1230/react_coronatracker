import React from 'react';

const Footer = () => {
    return (
        <div className='navbar navbar-light bg-primary'
            style={{ 'width': '100%' }}
        >

            <p className='navbar-brand text-light mx-auto'>&copy; Eushen Jungaya &amp; Michael Apuyan</p>
        </div>
    )
}

export default Footer;