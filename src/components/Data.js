const userList = [
    {
        id: 1,
        name: 'Stephen Curry',
        age: '31',
        gender: 'Male',
        condition: 'Healthy',
        job: 'Basketball Player',
        address: 'Oakland Street'
    }, {
        id: 2,
        name: 'Albert Nicolas',
        age: '28',
        gender: 'Male',
        condition: 'Healthy',
        job: 'Asian Cutie',
        address: 'Cutie Street'

    }, {
        id: 3,
        name: 'Revlon James',
        age: '34',
        gender: 'Male',
        condition: 'Healthy',
        job: 'Basketball player',
        address: 'LA Street'
    }, {
        id: 4,
        name: 'Carl Johnson',
        age: '52',
        gender: 'Male',
        condition: 'Diabetic',
        job: 'Businessman',
        address: 'Grove Street'
    },
    {
        id: 5,
        name: 'Tom Holland',
        age: '23',
        gender: 'Male',
        condition: 'Healthy',
        job: 'Actor',
        address: 'Arsenal Street'
    },
    {
        id: 6,
        name: 'Lola Remedios',
        age: '87',
        gender: 'Female',
        condition: 'Hypertension',
        job: 'Business Owner',
        address: 'Remedios Street'
    }, {
        id: 7,
        name: 'Margaret Bosco',
        age: '95',
        gender: 'Female',
        condition: 'Asthma',
        job: 'Retired Teacher',
        address: 'Italy Street'
    }, {
        id: 8,
        name: 'Evelyn Evernever',
        age: '67',
        gender: 'Female',
        condition: 'Healthy',
        job: 'Retired Engineer',
        address: 'Mechanic Street'
    }, {
        id: 9,
        name: 'Eliza Leonida',
        age: '100',
        gender: 'Female',
        condition: 'Healthy',
        job: 'Retired Neurosurgeon',
        address: 'Health Street'

    }
];

export default userList;