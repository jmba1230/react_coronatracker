import React, { useState, useEffect } from 'react';
// import Table
import { Table } from 'reactstrap';
// import Navbar
import Navbar from './components/Navbar';

// import Footer
import Footer from './components/Footer';

// import userList
import userList from './components/Data.js'
// import UserTable
import UserTable from './components/UserTable';

// import AddUserForm
import AddUserForm from './components/AddUserForm';

// import EditUserForm
import EditUserForm from './components/EditUserForm';

// import Moment JS
import moment from 'moment';




const App = () => {

  const [users, setUsers] = useState(userList);
  const [editing, setEditing] = useState(false);

  // TRY get current date time
  // const [time, setTime] = useState(new Date())

  // const changeTime = () => {
  //   setTime(new Date())
  // }


  // useEffect(() => {
  //   setInterval(() => {
  //     changeTime()
  //   }, 1000)
  //   return () => clearInterval()
  // })



  const initialUser = {
    id: null,
    name: '',
    age: '',
    gender: '',
    condition: '',
    job: '',
    address: ''
  };

  const [currentUser, setCurrentUser] = useState(initialUser);

  // TRY LANG API for COVID: Philippines
  const [country, setCountry] = useState([]);

  useEffect(() => {
    fetch('https://coronavirus-19-api.herokuapp.com/countries/Philippines')
      .then(res => res.json())
      .then(res => {
        setCountry(res)
      })
  }, []);


  // add user function
  const addUser = user => {
    user.id = users.length + 1;
    setUsers([...users, user]);
  }

  // delete user function
  const deleteUser = id => setUsers(users.filter(user => user.id !== id));

  // edit user function
  const editUser = (id, user) => {
    setEditing(true);
    setCurrentUser(user);
  }

  // update user function
  const updateUser = newUser => {
    setUsers(users.map(user => (user.id === currentUser.id ? newUser : user)));
    setEditing(false);
  }



  return (
    <>
      <Navbar />
      <div className='mx-auto py-5'>
        <h3><span
          className='badge badge-primary'
        >{country.country}</span> Statistics on COVID-19 as of: {moment().format('LLLL')}</h3>
        <Table dark>
          <thead>
            <tr>
              <th># of New Cases as of Today</th>
              <th># of New Deaths as of Today</th>
              <th>Total</th>
              <th>Cases</th>
              <th>Deaths</th>
              <th>Recovered</th>
            </tr>
          </thead>
          <tbody>
            <td>{country.todayCases}</td>
            <td>{country.todayDeaths}</td>
            <td>=</td>
            <td>{country.cases}</td>
            <td>{country.deaths}</td>
            <td>{country.recovered}</td>
          </tbody>
        </Table>
      </div>

      <div className='container align-items-center py-5'>
        <div className='row'>
          {editing ? (
            <div className='mx-auto'>
              <h2>Edit User</h2>
              <EditUserForm
                currentUser={currentUser}
                setEditing={setEditing}
                updateUser={updateUser}
              />
            </div>
          ) : (
              <div className='mx-auto'>
                <h2>Add User</h2>
                <AddUserForm
                  addUser={addUser}
                />
              </div>
            )}
        </div>
        <div className='row'>
          <div className='mx-auto py-5'>
            <h2>View Users</h2>
            <UserTable
              users={users}
              deleteUser={deleteUser}
              editUser={editUser}
            />
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default App;